function Cook (foodName) {

    var time = 0;
    switch(foodName) {
        case 'tea': 
            time = 1000;
            break;
        case 'meal': 
            time = 2000;
            break;
        case 'milk': 
            time = 200;
            break;  
    }

  return time;
}

function foodOrderingAndMaking(customerID, timeCounter, foodName) {

    return new Promise((response) => setTimeout( () =>
            response("cutomerID ||"+ customerID+ " ||foodName "+ foodName)
        ,timeCounter)
    )

}

function customer(customerID, foodName) {

  var timeCounter  = Cook(foodName)
  return foodOrderingAndMaking(customerID, timeCounter, foodName);

}

async function callCustomer(){
let op1 = await customer(1, 'tea');
console.log(op1);
let op2 = await customer(2, 'meal');
console.log(op2);
let op3 = await customer(3, 'milk');
console.log(op3);
let op4 = await customer(4, 'meal');
console.log(op4);
let op5 = await customer(5, 'meal');
console.log(op5);
}

callCustomer();
